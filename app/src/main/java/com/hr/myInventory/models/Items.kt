package com.hr.myInventory.models

data class Items(
    val id: String? = null,
    val name: String? = null,
    val description: String? = null,
    val location: String? = null,
    val imgUri: String? = null
)
