package com.hr.myInventory

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.hr.myInventory.databinding.FragmentAddBinding
import com.hr.myInventory.models.Items


class AddFragment : Fragment() {
    private var _binding : FragmentAddBinding? = null
    private  val binding get() = _binding!!

    private lateinit var firebaseRef : DatabaseReference
    private lateinit var storageRef : StorageReference

    private var uri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddBinding.inflate(inflater , container, false)
        firebaseRef = FirebaseDatabase.getInstance().getReference("items")
        storageRef = FirebaseStorage.getInstance().getReference("Images")

        binding.btnSend.setOnClickListener {
            saveData()
        }

        val pickImage = registerForActivityResult(ActivityResultContracts.GetContent()){
            binding.imgAdd.setImageURI(it)
            if (it != null){
                uri = it
            }
        }

        binding.btnSelectImage.setOnClickListener {
            pickImage.launch("image/*")
        }


        return binding.root
    }

    private fun saveData() {
        val name = binding.etItemName.text.toString()
        val description = binding.etItemDescription.text.toString()
        val location = binding.etItemLocation.text.toString()

        if (name.isEmpty()) binding.etItemName.error = "write a name"
        if (description.isEmpty()) binding.etItemDescription.error = "write a description"
        if (location.isEmpty()) binding.etItemDescription.error = "write a location"

        val itemId = firebaseRef.push().key!!
        var items : Items

        uri?.let{
            storageRef.child(itemId).putFile(it)
                .addOnSuccessListener { task->
                    task.metadata!!.reference!!.downloadUrl
                        .addOnSuccessListener { url ->
                            Toast.makeText(context, " Image stored successfully",Toast.LENGTH_SHORT).show()
                            val imgUrl = url.toString()


                            items = Items(itemId,name , description ,location, imgUrl)

                            firebaseRef.child(itemId).setValue(items)
                                .addOnCompleteListener{
                                    Toast.makeText(context, " data stored successfully",Toast.LENGTH_SHORT).show()
                                }
                                .addOnFailureListener{error ->
                                    Toast.makeText(context, "error ${error.message}",Toast.LENGTH_SHORT).show()
                                }


                        }
                }
        }


    }


}