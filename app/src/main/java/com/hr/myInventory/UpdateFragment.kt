package com.hr.myInventory

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.hr.myInventory.databinding.FragmentUpdateBinding
import com.hr.myInventory.models.Items
import com.squareup.picasso.Picasso


class UpdateFragment : Fragment() {

    private var _binding : FragmentUpdateBinding? = null
    private  val binding get() = _binding!!

    private val args : UpdateFragmentArgs by navArgs()

    private lateinit var firebaseRef : DatabaseReference
    private lateinit var storageRef : StorageReference

    private var uri: Uri? = null
    private var imageUrl : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUpdateBinding.inflate(inflater , container, false)

        firebaseRef = FirebaseDatabase.getInstance().getReference("items")
        storageRef = FirebaseStorage.getInstance().getReference("Images")

        val pickImage = registerForActivityResult(ActivityResultContracts.GetContent()){
            binding.imgUpdate.setImageURI(it)
            if (it != null){
                uri = it
            }
        }
        imageUrl = args.imageUrl

        binding.apply {
            etUpdateItem.setText(args.name)
            etUpdateDescription.setText(args.description)
            etUpdateLocation.setText(args.location)
            Picasso.get().load(imageUrl).into(imgUpdate)

            btnUpdate.setOnClickListener {
                updateData()
                findNavController().navigate(R.id.action_updateFragment_to_homeFragment)
            }

            imgUpdate.setOnClickListener {
                context?.let { context ->
                    MaterialAlertDialogBuilder(context)
                        .setTitle("changing the image")
                        .setMessage("please select the option")
                        .setPositiveButton("change image"){_,_->

                            pickImage.launch("image/*")
                        }
                        .setNegativeButton("delete image"){_,_->

                            imageUrl = null
                            binding.imgUpdate.setImageResource(R.drawable.ic_launcher_background)
                        }
                        .setNeutralButton("cancel"){_,_->}
                        .show()
                }
            }
        }

        return binding.root
    }

    private fun updateData() {
        val name = binding.etUpdateItem.text.toString()
        val description = binding.etUpdateDescription.text.toString()
        val location = binding.etUpdateLocation.text.toString()
        var items : Items



        if (uri != null){
            storageRef.child(args.id).putFile(uri!!)
                .addOnSuccessListener { task ->
                task.metadata!!.reference!!.downloadUrl
                    .addOnSuccessListener {url->
                    imageUrl = url.toString()
                    items = Items(args.id, name, description ,location, imageUrl)
                        firebaseRef.child(args.id).setValue(items)
                            .addOnCompleteListener{
                                Toast.makeText(context, " data updated successfully", Toast.LENGTH_SHORT).show()
                            }
                            .addOnFailureListener{
                                Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                            }

                    }

                }

        }
        if (uri == null){
            items = Items(args.id, name, description ,location, imageUrl)
            firebaseRef.child(args.id).setValue(items)
                .addOnCompleteListener{
                    Toast.makeText(context, " data updated successfully", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener{
                    Toast.makeText(context, "error ${it.message}", Toast.LENGTH_SHORT).show()
                }

        }
        if (imageUrl == null) storageRef.child(args.id).delete()
    }

    }

