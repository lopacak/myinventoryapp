package com.hr.myInventory

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.*
import com.hr.myInventory.adapter.RvItemsAdapter
import com.hr.myInventory.databinding.FragmentHomeBinding
import com.hr.myInventory.models.Items


class HomeFragment : Fragment() {
    private var _binding : FragmentHomeBinding? = null
    private  val binding get() = _binding!!

    private lateinit var itemsList: ArrayList<Items>
    private lateinit var  firebaseRef : DatabaseReference
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       _binding = FragmentHomeBinding.inflate(inflater , container, false)

        binding.btnAdd.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_addFragment)
        }
        firebaseRef = FirebaseDatabase.getInstance().getReference("items")
        itemsList = arrayListOf()

        fetchData()

        binding.rvItems.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this.context)
        }


        return binding.root
    }

    private fun fetchData() {
        firebaseRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                itemsList.clear()
                if (snapshot.exists()){
                    for (itemsnap in snapshot.children){
                        val items = itemsnap.getValue(Items::class.java)
                        itemsList.add(items!!)
                    }
                }
                val rvAdapter = RvItemsAdapter(itemsList)
                binding.rvItems.adapter = rvAdapter
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context," error : $error",Toast.LENGTH_SHORT).show()
            }

        })
    }


}